/* 
name: Joshua Dodanduwa
date: 15/03/2018
id: 10015281 */

//specifies the start and end of the timestable
let timesNum = 0


//allowing user to input a number for timestable
timesNum = Number(prompt("what timestable to do want?"))


console.log(`${timesNum} timestable:`)
console.log(" ")

//for loop
console.log("for loop")
for(i=1; i<=12; i++){

    console.log(`${i} x ${timesNum} = ${i*timesNum}`)

}

console.log(" ")

//while loop
console.log("while loop")
i = 1
while(i<=12){

    console.log(`${i} x ${timesNum} = ${i*timesNum}`)
    i++

}

console.log(" ")

//do while loop
console.log("do while loop")
i = 1
do{
    console.log(`${i} x ${timesNum} = ${i*timesNum}`)
    i++

} while(i<=12)
