/* 
name: Joshua Dodanduwa
date: 15/03/2018
id: 10015281 */

console.log ("Number     Square     Cube")
console.log ()
console.log()

//forloop
console.log("FOR LOOP")

for(i=1; i<=10; i++){
    
    console.log(` ${i}          ${i**2}          ${i**3}`)
}
console.log()


//while loop
console.log("WHILE LOOP")
i=1
while(i<=10) {

    console.log(` ${i}          ${i**2}          ${i**3}`)
    i++
}
console.log()


//do while loop
console.log("DO WHILE LOOP")
i=1
do {

    console.log(` ${i}          ${i**2}          ${i**3}`)
    i++
    
} while(i<=10)

