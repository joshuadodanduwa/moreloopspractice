/* 
name: Joshua Dodanduwa
date: 15/03/2018
id: 10015281 */

//specifies the start and end of the timestable
let startNum = 1
let endNum = 9

//headings for the timestable
console.log("N    N*10    N*100    N*1000")
console.log("-    ----    -----    ------")
console.log()

//for loop
console.log(" FOR LOOP ")

for (i = startNum; i <= endNum; i++) {

    console.log(`${i}    ${i*10}    ${i*100}    ${i*1000}` )
}

console.log()

//while loop
console.log(" WHILE LOOP ")

i = startNum
while(i<= endNum) {

    console.log(`${i}    ${i*10}    ${i*100}    ${i*1000}` )
    i++
}
console.log()

//do while loop
console.log(" DO WHILE LOOP ")

i = startNum
do {

    console.log(`${i}    ${i*10}    ${i*100}    ${i*1000}` )
    i++
}
while(i<= endNum)

